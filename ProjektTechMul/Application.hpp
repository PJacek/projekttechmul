#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "Test.hpp"
#include <cstdint>
#include <vector>
#include <memory>

class Application {
public:
	struct Result {
		explicit Result(std::unique_ptr<Test<uint8_t>> test) : test(std::move(test)) {}

		friend std::ostream& operator<<(std::ostream& os, const Result& p);
		std::ostream& printCSV(std::ostream& os) const;

		void addTimes() {
			renderingTime += test->renderingTime;
			registrationTime += test->registrationTime;
			mappingTime += test->mappingTime;
			unmappingTime += test->unmappingTime;
			unregistrationTime += test->unregistrationTime;
			kernelTime += test->kernelTime;
			CUDACopyTime += test->CUDACopyTime;
			OpenGLCopyTime += test->OpenGLCopyTime;
		};

		void calculateAverageTimes(int N) {
			renderingTime /= N;
			registrationTime /= N;
			mappingTime /= N;
			unmappingTime /= N;
			unregistrationTime /= N;
			kernelTime /= N;
			CUDACopyTime /= N;
			OpenGLCopyTime /= N;
		}

		std::unique_ptr<Test<uint8_t>> test;
		float renderingTime = 0.f;
		float registrationTime = 0.f;
		float mappingTime = 0.f;
		float unmappingTime = 0.f;
		float unregistrationTime = 0.f;
		float kernelTime = 0.f;
		float CUDACopyTime = 0.f;
		float OpenGLCopyTime = 0.f;
		bool equal = true;
	};

private:
	GLFWwindow* window = nullptr;
	GLuint vbo = 0;
	GLuint vao = 0;
	GLuint colorbuffer = 0;
	GLuint shaderProgram = 0;
	std::vector<std::unique_ptr<Result>> resultSet;
	unsigned width = 512, height = 512;

public:
	explicit Application();
	~Application();

	void draw(ptm::ShaderOutputFormat format) const;
	bool init(int width, int height);
	void loadShaders(const char* vertexFilePath, const char* fragmentFilePath);
	void run() const;
	static void printInfo();
	static GLFWerrorfun setErrorCallback(GLFWerrorfun func);
};
