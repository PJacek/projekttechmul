#pragma once
#include <GL/glew.h>
#include <driver_types.h>
#include <cuda_gl_interop.h>
#include <cuda_runtime_api.h>
#include "kernels.cuh"
#include <cstring>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include "utility.hpp"

class Application;

template <class T>
class Test {
private:
	GLuint texture = 0;
	cudaGraphicsResource_t resource = nullptr;
	cudaArray_t cudaArray = nullptr;
	unsigned height;
	unsigned width;
	unsigned size = 0;;
	T* colorOpenGL;
	T* colorCUDA;
	GLuint query;
	cudaEvent_t cudaStart;
	cudaEvent_t cudaStop;
	std::function<void(ptm::ShaderOutputFormat)> drawFunction;
public:
	GLint format;
	float renderingTime = 0.f;
	float registrationTime = 0.f;
	float mappingTime = 0.f;
	float unmappingTime = 0.f;
	float unregistrationTime = 0.f;
	float kernelTime = 0.f;
	float CUDACopyTime = 0.f;
	float OpenGLCopyTime = 0.f;
	Test(unsigned int width, unsigned int height, GLint format, std::function<void(ptm::ShaderOutputFormat)> drawFunction);
	~Test();
	void registerResource();
	void bindTextureToFramebuffer() const;
	void readFromOpenGL();
	void mapResource();
	void readFromCUDA();
	void runKernel();
	void unmapResource();
	void unregisterResource();
	bool compare();
	void draw();
};

template <class T>
Test<T>::Test(unsigned width, unsigned height, GLint format, std::function<void(ptm::ShaderOutputFormat)> drawFunction) : height(height), width(width), format(format) {
	this->drawFunction = drawFunction;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, ptm::format2Base.at(format), GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);

	switch (format) {
		case GL_R8:
		case GL_R8UI:
		case GL_R8I:
			size = 1 * 1 * width * height;
			break;
		case GL_R16:
		case GL_R16F:
		case GL_R16UI:
		case GL_R16I:
			size = 1 * 2 * width * height;
			break;
		case GL_R32F:
		case GL_R32UI:
		case GL_R32I:
			size = 1 * 4 * width * height;
			break;
		case GL_RG8:
		case GL_RG8UI:
		case GL_RG8I:
			size = 2 * 1 * width * height;
			break;
		case GL_RG16:
		case GL_RG16F:
		case GL_RG16UI:
		case GL_RG16I:
			size = 2 * 2 * width * height;
			break;
		case GL_RG32F:
		case GL_RG32UI:
		case GL_RG32I:
			size = 2 * 4 * width * height;
			break;
		case GL_RGBA8:
		case GL_RGBA8UI:
		case GL_RGBA8I:
			size = 4 * 1 * width * height;
			break;
		case GL_RGBA16:
		case GL_RGBA16F:
		case GL_RGBA16UI:
		case GL_RGBA16I:
			size = 4 * 2 * width * height;
			break;
		case GL_RGBA32F:
		case GL_RGBA32UI:
		case GL_RGBA32I:
			size = 4 * 4 * width * height;
	}
	colorOpenGL = new T[size];
	colorCUDA = new T[size];
	glGenQueries(1, &query);
	cudaEventCreate(&cudaStart);
	cudaEventCreate(&cudaStop);
}

template <class T>
Test<T>::~Test() {
	cudaEventDestroy(cudaStop);
	cudaEventDestroy(cudaStart);
	glDeleteQueries(1, &query);
	delete[] colorCUDA;
	delete[] colorOpenGL;
	glDeleteTextures(1, &texture);
}

template <class T>
void Test<T>::registerResource() {
	cudaEventRecord(cudaStart);
	cudaGraphicsGLRegisterImage(&resource, texture, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore);
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&registrationTime, cudaStart, cudaStop);
}

template <class T>
void Test<T>::bindTextureToFramebuffer() const {
	glFramebufferTexture2D(GL_FRAMEBUFFER, ptm::format2Att.at(format), GL_TEXTURE_2D, texture, 0);
}

template <class T>
void Test<T>::readFromOpenGL() {
	glFinish();
	glPixelStorei(GL_PACK_ALIGNMENT, 4);
	glBeginQuery(GL_TIME_ELAPSED, query);
	glReadBuffer(ptm::format2Att.at(format));
	glReadPixels(0, 0, width, height, ptm::format2Base.at(format), ptm::format2Type.at(format), colorOpenGL);
	glEndQuery(GL_TIME_ELAPSED);
	GLuint time;
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &time);
	OpenGLCopyTime = time / 1000000.f;
}

template <class T>
void Test<T>::mapResource() {
	cudaEventRecord(cudaStart);
	cudaGraphicsMapResources(1, &resource);
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&mappingTime, cudaStart, cudaStop);
	cudaGraphicsSubResourceGetMappedArray(&cudaArray, resource, 0, 0);
}

template <class T>
void Test<T>::readFromCUDA() {
	cudaStreamSynchronize(nullptr);
	cudaEventRecord(cudaStart);
	cudaMemcpyFromArray(colorCUDA, cudaArray, 0, 0, size, cudaMemcpyDeviceToHost);
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&CUDACopyTime, cudaStart, cudaStop);
}

template <class T>
void Test<T>::runKernel() {
	cudaResourceDesc desc;
	desc.resType = cudaResourceTypeArray;
	desc.res.array.array = cudaArray;
	cudaSurfaceObject_t surface;
	cudaCreateSurfaceObject(&surface, &desc);

	cudaEventRecord(cudaStart);
	invokeKernel(surface, dim3(width, height), format);
	cudaDeviceSynchronize();
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&kernelTime, cudaStart, cudaStop);

	cudaDestroySurfaceObject(surface);
}

template <class T>
void Test<T>::unmapResource() {
	cudaEventRecord(cudaStart);
	cudaGraphicsUnmapResources(1, &resource);
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&unmappingTime, cudaStart, cudaStop);
}

template <class T>
void Test<T>::unregisterResource() {
	cudaEventRecord(cudaStart);
	cudaGraphicsUnregisterResource(resource);
	cudaEventRecord(cudaStop);
	cudaEventSynchronize(cudaStop);
	cudaEventElapsedTime(&unregistrationTime, cudaStart, cudaStop);
}

template <class T>
bool Test<T>::compare() {
	return std::equal(colorOpenGL, colorOpenGL + size, stdext::checked_array_iterator<T*>(colorCUDA, size));
}

template <class T>
void Test<T>::draw() {
	glBeginQuery(GL_TIME_ELAPSED, query);
	drawFunction(ptm::format2Out.at(format));
	glEndQuery(GL_TIME_ELAPSED);
	GLuint time;
	glGetQueryObjectuiv(query, GL_QUERY_RESULT, &time);
	renderingTime = time / 1000000.f;
}
