#include "kernels.cuh"
#include <limits>
#include <cstdint>

template <class T, class M>
__global__ void invert4(cudaSurfaceObject_t s, dim3 texDim, char size, M max) {
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x < texDim.x && y < texDim.y) {
		T data = surf2Dread<T>(s, x * size, y);
		T inverted;
		inverted.w = data.w;
		inverted.z = max - data.z;
		inverted.y = max - data.y;
		inverted.x = max - data.x;	
		surf2Dwrite(inverted, s, x * size, y);
	}
}

template <class T, class M>
__global__ void invert2(cudaSurfaceObject_t s, dim3 texDim, char size, M max) {
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x < texDim.x && y < texDim.y) {
		T data = surf2Dread<T>(s, x * size, y);
		T inverted;
		inverted.y = max - data.y;
		inverted.x = max - data.x;
		surf2Dwrite(inverted, s, x * size, y);
	}
}

template <class T, class M>
__global__ void invert1(cudaSurfaceObject_t s, dim3 texDim, char size, M max) {
	unsigned int x = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y * blockDim.y + threadIdx.y;

	if (x < texDim.x && y < texDim.y) {
		T data = surf2Dread<T>(s, x * size, y);
		T inverted;
		inverted.x = max - data.x;
		surf2Dwrite(inverted, s, x * size, y);
	}
}

void invokeKernel(cudaSurfaceObject_t surface, dim3 texDim, GLuint format) {
	dim3 thread(32, 32);
	dim3 block(texDim.x / thread.x, texDim.y / thread.y);
	switch (format) {
		case GL_R8:
			invert1<uchar1, uint8_t><<< block, thread >>>(surface, texDim, 1, std::numeric_limits<uint8_t>::max());
			break;
		case GL_R8UI:
			invert1<uchar1, uint8_t><<< block, thread >>>(surface, texDim, 1, std::numeric_limits<uint8_t>::max());
			break;
		case GL_R8I:
			invert1<char1, int8_t><<< block, thread >>>(surface, texDim, 1, -1);
			break;
		case GL_R16:
			invert1<ushort1, uint16_t><<< block, thread >>>(surface, texDim, 2, std::numeric_limits<uint16_t>::max());
			break;
		case GL_R16F:
			break;
		case GL_R16UI:
			invert1<ushort1, uint16_t><<< block, thread >>>(surface, texDim, 2, std::numeric_limits<uint16_t>::max());
			break;
		case GL_R16I:
			invert1<short1, int16_t><<< block, thread >>>(surface, texDim, 2, -1);
			break;
		case GL_R32F:
			invert1<float1, float><<< block, thread >>>(surface, texDim, 4, 1.f);
			break;
		case GL_R32UI:
			invert1<uint1, uint32_t><<< block, thread >>>(surface, texDim, 4, std::numeric_limits<uint32_t>::max());
			break;
		case GL_R32I:
			invert1<int1, int32_t><<< block, thread >>>(surface, texDim, 4, -1);
			break;
		case GL_RG8:
			invert2<uchar2, uint8_t><<< block, thread >>>(surface, texDim, 2, std::numeric_limits<uint8_t>::max());
			break;
		case GL_RG8UI:
			invert2<uchar2, uint8_t><<< block, thread >>>(surface, texDim, 2, std::numeric_limits<uint8_t>::max());
			break;
		case GL_RG8I:
			invert2<char2, int8_t><<< block, thread >>>(surface, texDim, 2, -1);
			break;
		case GL_RG16:
			invert2<ushort2, uint16_t><<< block, thread >>>(surface, texDim, 4, std::numeric_limits<uint16_t>::max());
			break;
		case GL_RG16F:
			break;
		case GL_RG16UI:
			invert2<ushort2, uint16_t><<< block, thread >>>(surface, texDim, 4, std::numeric_limits<uint16_t>::max());
			break;
		case GL_RG16I:
			invert2<short2, int16_t><<< block, thread >>>(surface, texDim, 4, -1);
			break;
		case GL_RG32F:
			invert2<float2, float><<< block, thread >>>(surface, texDim, 8, 1.f);
			break;
		case GL_RG32UI:
			invert2<uint2, uint32_t><<< block, thread >>>(surface, texDim, 8, std::numeric_limits<uint32_t>::max());
			break;
		case GL_RG32I:
			invert2<int2, int32_t><<< block, thread >>>(surface, texDim, 8, -1);
			break;
		case GL_RGBA8:
			invert4<uchar4, uint8_t><<< block, thread >>>(surface, texDim, 4, std::numeric_limits<uint8_t>::max());
			break;
		case GL_RGBA8UI:
			invert4<uchar4, uint8_t><<< block, thread >>>(surface, texDim, 4, std::numeric_limits<uint8_t>::max());
			break;
		case GL_RGBA8I:
			invert4<char4, int8_t><<< block, thread >>>(surface, texDim, 4, -1);
			break;
		case GL_RGBA16:
			invert4<ushort4, uint16_t><<< block, thread >>>(surface, texDim, 8, std::numeric_limits<uint16_t>::max());
			break;
		case GL_RGBA16F:
			break;
		case GL_RGBA16UI:
			invert4<ushort4, uint16_t><<< block, thread >>>(surface, texDim, 8, std::numeric_limits<uint16_t>::max());
			break;
		case GL_RGBA16I:
			invert4<short4, int16_t><<< block, thread >>>(surface, texDim, 8, -1);
			break;
		case GL_RGBA32F:
			invert4<float4, float><<< block, thread >>>(surface, texDim, 16, 1.f);
			break;
		case GL_RGBA32UI:
			invert4<uint4, uint32_t><<< block, thread >>>(surface, texDim, 16, std::numeric_limits<uint32_t>::max());
			break;
		case GL_RGBA32I:
			invert4<int4, int32_t><<< block, thread >>>(surface, texDim, 16, -1);
			break;
	}
}
