#include "Application.hpp"
#include <glm/gtx/transform2.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cuda_gl_interop.h>
#include "consolecolor.hpp"
#include "utility.hpp"

Application::Application(){}

GLFWerrorfun Application::setErrorCallback(GLFWerrorfun func) {
	return glfwSetErrorCallback(func);
}

bool Application::init(int width, int height) {
	this->width = width;
	this->height = height;
	if (!glfwInit()) {
		std::cerr << "Could not start GLFW3\n" << std::endl;
		return false;
	}
	ptm::gpuGLDeviceInit(ptm::gpuGetMaxGflopsDeviceId());

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);

	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(width, height, "", nullptr, nullptr);

	if (!window) {
		std::cerr << red << "Could not open window with GLFW3\n" << white << std::endl;
		glfwTerminate();
		return false;
	}

	// Ustawienie kontekstu na okno
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	ptm::printGLErrors("GLEW Hax");
	if (err != GLEW_OK) {
		std::cerr << red << err << ": Could not start GLEW\n" << white << std::endl;
		return false;
	}

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	float points[] = {
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f
	};

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

	float colors[] = {
		0.583f, 0.771f, 0.014f,
		0.609f, 0.115f, 0.436f,
		0.327f, 0.483f, 0.844f,
		0.822f, 0.569f, 0.201f,
		0.435f, 0.602f, 0.223f,
		0.310f, 0.747f, 0.185f,
		0.597f, 0.770f, 0.761f,
		0.559f, 0.436f, 0.730f,
		0.359f, 0.583f, 0.152f,
		0.483f, 0.596f, 0.789f,
		0.559f, 0.861f, 0.639f,
		0.195f, 0.548f, 0.859f,
		0.014f, 0.184f, 0.576f,
		0.771f, 0.328f, 0.970f,
		0.406f, 0.615f, 0.116f,
		0.676f, 0.977f, 0.133f,
		0.971f, 0.572f, 0.833f,
		0.140f, 0.616f, 0.489f,
		0.997f, 0.513f, 0.064f,
		0.945f, 0.719f, 0.592f,
		0.543f, 0.021f, 0.978f,
		0.279f, 0.317f, 0.505f,
		0.167f, 0.620f, 0.077f,
		0.347f, 0.857f, 0.137f,
		0.055f, 0.953f, 0.042f,
		0.714f, 0.505f, 0.345f,
		0.783f, 0.290f, 0.734f,
		0.722f, 0.645f, 0.174f,
		0.302f, 0.455f, 0.848f,
		0.225f, 0.587f, 0.040f,
		0.517f, 0.713f, 0.338f,
		0.053f, 0.959f, 0.120f,
		0.393f, 0.621f, 0.362f,
		0.673f, 0.211f, 0.457f,
		0.820f, 0.883f, 0.371f,
		0.982f, 0.099f, 0.879f
	};

	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R8, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R8UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R8I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R16, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R16F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R16UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R16I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R32F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R32UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_R32I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG8, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG8UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG8I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG16, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG16F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG16UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG16I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG32F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG32UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RG32I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA8, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA8UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA8I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA16, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA16F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA16UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA16I, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA32F, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA32UI, std::bind(&Application::draw, this, std::placeholders::_1))));
	resultSet.push_back(std::make_unique<Result>(std::make_unique<Test<uint8_t>>(width, height, GL_RGBA32I, std::bind(&Application::draw, this, std::placeholders::_1))));

	return true;
}

Application::~Application() {
	glfwDestroyWindow(window);
	glfwTerminate();
}

void Application::loadShaders(const char* vertexFilePath, const char* fragmentFilePath) {
	// Create shaders
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string vertexShaderCode;
	std::ifstream vertexShaderStream(vertexFilePath, std::ios::in);
	if (vertexShaderStream.is_open()) {
		std::string line = "";
		while (std::getline(vertexShaderStream, line))
			vertexShaderCode += "\n" + line;
		vertexShaderStream.close();
	} else {
		std::cerr << red << "Cannot open " << vertexFilePath << white << std::endl;
	}

	// Read the Fragment Shader code from the file
	std::string fragmentShaderCode;
	std::ifstream fragmentShaderStream(fragmentFilePath, std::ios::in);
	if (fragmentShaderStream.is_open()) {
		std::string line = "";
		while (std::getline(fragmentShaderStream, line))
			fragmentShaderCode += "\n" + line;
		fragmentShaderStream.close();
	} else {
		std::cerr << red << "Cannot open " << fragmentFilePath << white << std::endl;
	}

	GLint result;
	int infoLogLength;

	// Compile Vertex Shader
	std::cout << "Compiling shader: " << vertexFilePath << std::endl;
	const char* vertexSource = vertexShaderCode.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, nullptr);
	glCompileShader(vertexShader);

	// Check Vertex Shader
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		std::vector<char> vertexShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(vertexShader, infoLogLength, nullptr, &vertexShaderErrorMessage[0]);
		std::cerr << &vertexShaderErrorMessage[0] << std::endl;
	}

	// Compile Fragment Shader
	std::cout << "Compiling shader: " << fragmentFilePath << std::endl;
	const char* fragmentSource = fragmentShaderCode.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, nullptr);
	glCompileShader(fragmentShader);

	// Check Fragment Shader
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		std::vector<char> fragmentShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(fragmentShader, infoLogLength, nullptr, &fragmentShaderErrorMessage[0]);
		std::cerr << &fragmentShaderErrorMessage[0] << std::endl;
	}

	// Link the program
	std::cout << "Linking program" << std::endl;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// Check the program
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &result);
	glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		std::vector<char> programErrorMessage(infoLogLength + 1);
		glGetProgramInfoLog(shaderProgram, infoLogLength, nullptr, &programErrorMessage[0]);
		std::cerr << &programErrorMessage[0] << std::endl;
	}

	glDetachShader(shaderProgram, vertexShader);
	glDetachShader(shaderProgram, fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Application::draw(ptm::ShaderOutputFormat format) const {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);
	glBindVertexArray(vao);
	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), static_cast<float>(width) / static_cast<float>(height), 0.1f, 100.0f);
	glm::mat4 View = glm::lookAt(
		glm::vec3(4, 3, 3), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 0), // and looks at the origin
		glm::vec3(0, 1, 0) // Head is up (set to 0,-1,0 to look upside-down)
	);
	glm::mat4 Model = glm::mat4(1.0f);
	glm::mat4 mvp = Projection * View * Model;
	GLuint MatrixID = glGetUniformLocation(shaderProgram, "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mvp[0][0]);
	GLuint OutFormatID = glGetUniformLocation(shaderProgram, "format");
	glUniform1i(OutFormatID, static_cast<int>(format));
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
}

void Application::run() const {
	GLuint framebuffer;
	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

	glViewport(0, 0, width, height);
	GLenum drawBuffers[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, drawBuffers);

	auto file = std::ofstream("output.csv", std::ofstream::trunc);
	file << "Format, Renderowanie, Kopia z CUDA, Kopia z OpenGL, Kopia proprawna, Rejestracja, Mapowanie, Kernel CUDA, Demapowanie, Derejestracja" << std::endl;
	for (auto it = resultSet.begin(); it != resultSet.end(); ++it) {
		int N = 100;
		for (auto i = 1; i < N; ++i) {
			(*it)->test->registerResource();
			(*it)->test->bindTextureToFramebuffer();
			(*it)->test->draw();
			(*it)->test->readFromOpenGL();
			(*it)->test->mapResource();
			(*it)->test->readFromCUDA();
			(*it)->equal &= (*it)->test->compare();
			(*it)->test->runKernel();
			(*it)->test->unmapResource();
			(*it)->test->unregisterResource();
			(*it)->addTimes();
		}
		(*it)->calculateAverageTimes(N);
		std::cout << **it;
		(*it)->printCSV(file);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glDeleteFramebuffers(1, &framebuffer);

	system("PAUSE");
}

void Application::printInfo() {
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	std::cout << "Renderer: " << renderer << std::endl;
	std::cout << "OpenGL version: " << version << std::endl << std::endl;
}

std::ostream& operator<<(std::ostream& os, const Application::Result& r) {
	os << "Format: " << ptm::format2String.at(r.test->format) << std::endl;
	os << "Renderowanie: " << r.renderingTime << std::endl;
	os << "Kopia z CUDA: " << r.CUDACopyTime << std::endl;
	os << "Kopia z OpenGL: " << r.OpenGLCopyTime << std::endl;
	os << "Kopia proprawna: " << (r.equal ? "T" : "N") << std::endl;
	os << "Rejestracja: " << r.registrationTime << std::endl;
	os << "Mapowanie: " << r.mappingTime << std::endl;
	os << "Kernel CUDA: " << r.kernelTime << std::endl;
	os << "Demapowanie: " << r.unmappingTime << std::endl;
	os << "Derejestracja: " << r.unregistrationTime << std::endl << std::endl;
	return os;
}

std::ostream& Application::Result::printCSV(std::ostream& os) const {
	os <<  ptm::format2String.at(this->test->format) << ", ";
	os <<  this->renderingTime << ", ";
	os <<  this->CUDACopyTime << ", ";
	os <<  this->OpenGLCopyTime << ", ";
	os <<  (this->equal ? "T" : "N") << ", ";
	os <<  this->registrationTime << ", ";
	os <<  this->mappingTime << ", ";
	os <<  this->kernelTime << ", ";
	os <<  this->unmappingTime << ", ";
	os <<  this->unregistrationTime << std::endl;
	return os;
}
