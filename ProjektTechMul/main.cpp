#include "Application.hpp"
#include <iostream>

int main() {
	Application app;

	if (!app.init(512, 512))
		return EXIT_FAILURE;

	app.setErrorCallback([](int error, const char* description) {
		std::cerr << error << " : " << description << std::endl;
	});

	app.printInfo();
	app.loadShaders("shaders/vertex_shader.glsl", "shaders/fragment_shader.glsl");

	app.run();

	return EXIT_SUCCESS;
}
