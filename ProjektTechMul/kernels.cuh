#include <cuda_runtime.h>
#include <GL/glew.h>

template <class T>
__global__ void invert(cudaSurfaceObject_t s, dim3 texDim);
void invokeKernel(cudaSurfaceObject_t surface, dim3 texDim, GLuint format);
