#version 400
in vec3 fragmentColour;
layout(location = 0) out vec4 colour;
layout(location = 1) out uvec4 colour_ui;
layout(location = 2) out ivec4 colour_i;
uniform int format;

void main () {
	switch (format) {
		case 0: 
			colour = vec4(fragmentColour, 1.0);
			break;
		case 1:
			colour_ui = uvec4(fragmentColour.x * 255, fragmentColour.y * 255, fragmentColour.z * 255, 255);
			break;
		case 2:
			colour_ui = uvec4(fragmentColour.x * 65535, fragmentColour.y * 65535, fragmentColour.z * 65535, 65535);
			break;
		case 3:
			colour_ui = uvec4(fragmentColour.x * 4294967295, fragmentColour.y * 4294967295, fragmentColour.z * 4294967295, 4294967295);
			break;
		case 4:
			colour_i = ivec4(fragmentColour.x * 255 - 128, fragmentColour.y * 255 - 128, fragmentColour.z * 255 - 128, 127);
			break;
		case 5:
			colour_i = ivec4(fragmentColour.x * 65535 - 32768, fragmentColour.y * 65535 - 32768, fragmentColour.z * 65535 - 32768, 32767);
			break;
		case 6:
			colour_i = ivec4(fragmentColour.x * 4294967295 - 2147483648, fragmentColour.y * 4294967295 - 2147483648, fragmentColour.z * 4294967295 - 2147483648, 2147483647);
			break;
	}
}

