#include "utility.hpp"

namespace ptm
{
	const std::map<int, int> format2Base = {
		{ GL_R8, GL_RED },
		{ GL_R8UI, GL_RED_INTEGER },
		{ GL_R8I, GL_RED_INTEGER },
		{ GL_R16, GL_RED },
		{ GL_R16F, GL_RED },
		{ GL_R16UI, GL_RED_INTEGER },
		{ GL_R16I, GL_RED_INTEGER },
		{ GL_R32F, GL_RED },
		{ GL_R32UI, GL_RED_INTEGER },
		{ GL_R32I, GL_RED_INTEGER },
		{ GL_RG8, GL_RG },
		{ GL_RG8UI, GL_RG_INTEGER },
		{ GL_RG8I, GL_RG_INTEGER },
		{ GL_RG16, GL_RG },
		{ GL_RG16F, GL_RG },
		{ GL_RG16UI, GL_RG_INTEGER },
		{ GL_RG16I, GL_RG_INTEGER },
		{ GL_RG32F, GL_RG },
		{ GL_RG32UI, GL_RG_INTEGER },
		{ GL_RG32I, GL_RG_INTEGER },
		{ GL_RGBA8, GL_RGBA },
		{ GL_RGBA8UI, GL_RGBA_INTEGER },
		{ GL_RGBA8I, GL_RGBA_INTEGER },
		{ GL_RGBA16, GL_RGBA },
		{ GL_RGBA16F, GL_RGBA },
		{ GL_RGBA16UI, GL_RGBA_INTEGER },
		{ GL_RGBA16I, GL_RGBA_INTEGER },
		{ GL_RGBA32F, GL_RGBA },
		{ GL_RGBA32UI, GL_RGBA_INTEGER },
		{ GL_RGBA32I, GL_RGBA_INTEGER }
	};

	const std::map<int, int> format2Type = {
		{ GL_R8, GL_UNSIGNED_BYTE },
		{ GL_R8UI, GL_UNSIGNED_BYTE },
		{ GL_R8I, GL_BYTE },
		{ GL_R16, GL_UNSIGNED_SHORT },
		{ GL_R16F, GL_HALF_FLOAT },
		{ GL_R16UI, GL_UNSIGNED_SHORT },
		{ GL_R16I, GL_SHORT },
		{ GL_R32F, GL_FLOAT },
		{ GL_R32UI, GL_UNSIGNED_INT },
		{ GL_R32I, GL_INT },
		{ GL_RG8, GL_UNSIGNED_BYTE },
		{ GL_RG8UI, GL_UNSIGNED_BYTE },
		{ GL_RG8I, GL_BYTE },
		{ GL_RG16, GL_UNSIGNED_SHORT },
		{ GL_RG16F, GL_HALF_FLOAT },
		{ GL_RG16UI, GL_UNSIGNED_SHORT },
		{ GL_RG16I, GL_SHORT },
		{ GL_RG32F, GL_FLOAT },
		{ GL_RG32UI, GL_UNSIGNED_INT },
		{ GL_RG32I, GL_INT },
		{ GL_RGBA8, GL_UNSIGNED_BYTE },
		{ GL_RGBA8UI, GL_UNSIGNED_BYTE },
		{ GL_RGBA8I, GL_BYTE },
		{ GL_RGBA16, GL_UNSIGNED_SHORT },
		{ GL_RGBA16F, GL_HALF_FLOAT },
		{ GL_RGBA16UI, GL_UNSIGNED_SHORT },
		{ GL_RGBA16I, GL_SHORT },
		{ GL_RGBA32F, GL_FLOAT },
		{ GL_RGBA32UI, GL_UNSIGNED_INT },
		{ GL_RGBA32I, GL_INT }
	};

	const std::map<int, int> format2Att = {
		{ GL_R8, GL_COLOR_ATTACHMENT0 },
		{ GL_R8UI, GL_COLOR_ATTACHMENT1 },
		{ GL_R8I, GL_COLOR_ATTACHMENT2 },
		{ GL_R16, GL_COLOR_ATTACHMENT0 },
		{ GL_R16F, GL_COLOR_ATTACHMENT0 },
		{ GL_R16UI, GL_COLOR_ATTACHMENT1 },
		{ GL_R16I, GL_COLOR_ATTACHMENT2 },
		{ GL_R32F, GL_COLOR_ATTACHMENT0 },
		{ GL_R32UI, GL_COLOR_ATTACHMENT1 },
		{ GL_R32I, GL_COLOR_ATTACHMENT2 },
		{ GL_RG8, GL_COLOR_ATTACHMENT0 },
		{ GL_RG8UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RG8I, GL_COLOR_ATTACHMENT2 },
		{ GL_RG16, GL_COLOR_ATTACHMENT0 },
		{ GL_RG16F, GL_COLOR_ATTACHMENT0 },
		{ GL_RG16UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RG16I, GL_COLOR_ATTACHMENT2 },
		{ GL_RG32F, GL_COLOR_ATTACHMENT0 },
		{ GL_RG32UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RG32I, GL_COLOR_ATTACHMENT2 },
		{ GL_RGBA8, GL_COLOR_ATTACHMENT0 },
		{ GL_RGBA8UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RGBA8I, GL_COLOR_ATTACHMENT2 },
		{ GL_RGBA16, GL_COLOR_ATTACHMENT0 },
		{ GL_RGBA16F, GL_COLOR_ATTACHMENT0 },
		{ GL_RGBA16UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RGBA16I, GL_COLOR_ATTACHMENT2 },
		{ GL_RGBA32F, GL_COLOR_ATTACHMENT0 },
		{ GL_RGBA32UI, GL_COLOR_ATTACHMENT1 },
		{ GL_RGBA32I, GL_COLOR_ATTACHMENT2 }
	};

	const std::map<int, ShaderOutputFormat> format2Out = {
		{ GL_R8, ptm::ShaderOutputFormat::Float },
		{ GL_R8UI, ptm::ShaderOutputFormat::Unsigned8 },
		{ GL_R8I, ptm::ShaderOutputFormat::Signed8 },
		{ GL_R16, ptm::ShaderOutputFormat::Float },
		{ GL_R16F, ptm::ShaderOutputFormat::Float },
		{ GL_R16UI, ptm::ShaderOutputFormat::Unsigned16 },
		{ GL_R16I, ptm::ShaderOutputFormat::Signed16 },
		{ GL_R32F, ptm::ShaderOutputFormat::Float },
		{ GL_R32UI, ptm::ShaderOutputFormat::Unsigned32 },
		{ GL_R32I, ptm::ShaderOutputFormat::Signed32 },
		{ GL_RG8, ptm::ShaderOutputFormat::Float },
		{ GL_RG8UI, ptm::ShaderOutputFormat::Unsigned8 },
		{ GL_RG8I, ptm::ShaderOutputFormat::Signed8 },
		{ GL_RG16, ptm::ShaderOutputFormat::Float },
		{ GL_RG16F, ptm::ShaderOutputFormat::Float },
		{ GL_RG16UI, ptm::ShaderOutputFormat::Unsigned16 },
		{ GL_RG16I, ptm::ShaderOutputFormat::Signed16 },
		{ GL_RG32F, ptm::ShaderOutputFormat::Float },
		{ GL_RG32UI, ptm::ShaderOutputFormat::Unsigned32 },
		{ GL_RG32I, ptm::ShaderOutputFormat::Signed32 },
		{ GL_RGBA8, ptm::ShaderOutputFormat::Float },
		{ GL_RGBA8UI, ptm::ShaderOutputFormat::Unsigned8 },
		{ GL_RGBA8I, ptm::ShaderOutputFormat::Signed8 },
		{ GL_RGBA16, ptm::ShaderOutputFormat::Float },
		{ GL_RGBA16F, ptm::ShaderOutputFormat::Float },
		{ GL_RGBA16UI, ptm::ShaderOutputFormat::Unsigned16 },
		{ GL_RGBA16I, ptm::ShaderOutputFormat::Signed16 },
		{ GL_RGBA32F, ptm::ShaderOutputFormat::Float },
		{ GL_RGBA32UI, ptm::ShaderOutputFormat::Unsigned32 },
		{ GL_RGBA32I, ptm::ShaderOutputFormat::Signed32 }
	};

	const std::map<int, std::string> format2String = {
		{ GL_R8, "GL_R8" },
		{ GL_R8UI, "GL_R8UI" },
		{ GL_R8I, "GL_R8I" },
		{ GL_R16, "GL_R16" },
		{ GL_R16F, "GL_R16F" },
		{ GL_R16UI, "GL_R16UI" },
		{ GL_R16I, "GL_R16I" },
		{ GL_R32F, "GL_R32F" },
		{ GL_R32UI, "GL_R32UI" },
		{ GL_R32I, "GL_R32I" },
		{ GL_RG8, "GL_RG8" },
		{ GL_RG8UI, "GL_RG8UI" },
		{ GL_RG8I, "GL_RG8I" },
		{ GL_RG16, "GL_RG16" },
		{ GL_RG16F, "GL_RG16F" },
		{ GL_RG16UI, "GL_RG16UI" },
		{ GL_RG16I, "GL_RG16I" },
		{ GL_RG32F, "GL_RG32F" },
		{ GL_RG32UI, "GL_RG32UI" },
		{ GL_RG32I, "GL_RG32I" },
		{ GL_RGBA8, "GL_RGBA8" },
		{ GL_RGBA8UI, "GL_RGBA8UI" },
		{ GL_RGBA8I, "GL_RGBA8I" },
		{ GL_RGBA16, "GL_RGBA16" },
		{ GL_RGBA16F, "GL_RGBA16F" },
		{ GL_RGBA16UI, "GL_RGBA16UI" },
		{ GL_RGBA16I, "GL_RGBA16I" },
		{ GL_RGBA32F, "GL_RGBA32F" },
		{ GL_RGBA32UI, "GL_RGBA32UI" },
		{ GL_RGBA32I, "GL_RGBA32I" }
	};

	void printGLErrors(const char* description) {
		GLenum error;
		std::cerr << "Errors after: " << description << red << std::endl;
		while ((error = glGetError()) != GL_NO_ERROR) {
			switch (error) {
				case GL_NO_ERROR:
					//No error has been recorded.
					//The value of this symbolic constant is guaranteed to be 0.
					std::cerr << "GL_NO_ERROR" << std::endl;
					break;
				case GL_INVALID_ENUM:
					//An unacceptable value is specified for an enumerated argument. Ignored
					std::cerr << "GL_INVALID_ENUM" << std::endl;
					break;
				case GL_INVALID_VALUE:
					//A numeric argument is out of range. Ignored
					std::cerr << "GL_INVALID_VALUE" << std::endl;
					break;
				case GL_INVALID_OPERATION:
					//The specified operation is not allowed in the current state. Ignored
					std::cerr << "GL_INVALID_OPERATION" << std::endl;
					break;
				case 1286:
					//The framebuffer object is not complete. Ignored
					std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION?" << std::endl;
					break;
				case GL_OUT_OF_MEMORY:
					//There is not enough memory left to execute the command. Undefined
					std::cerr << "GL_OUT_OF_MEMORY" << std::endl;
					break;
				case GL_STACK_UNDERFLOW:
					//An attempt has been made to perform an operation that would
					//cause an internal stack to underflow.
					std::cerr << "GL_STACK_UNDERFLOW" << std::endl;
					break;
				case GL_STACK_OVERFLOW:
					//An attempt has been made to perform an operation that would
					//cause an internal stack to overflow.
					std::cerr << "GL_STACK_OVERFLOW" << std::endl;
					break;
				default:
					std::cerr << "Unknown error: " << error << std::endl;
					break;
			}
		}
		std::cerr << white;
	}

	inline int _ConvertSMVer2Cores(int major, int minor) {
		// Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
		typedef struct {
			int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
			int Cores;
		} sSMtoCores;

		sSMtoCores nGpuArchCoresPerSM[] =
			{
				{ 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
				{ 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
				{ 0x30, 192 }, // Kepler Generation (SM 3.0) GK10x class
				{ 0x32, 192 }, // Kepler Generation (SM 3.2) GK10x class
				{ 0x35, 192 }, // Kepler Generation (SM 3.5) GK11x class
				{ 0x37, 192 }, // Kepler Generation (SM 3.7) GK21x class
				{ 0x50, 128 }, // Maxwell Generation (SM 5.0) GM10x class
				{ 0x52, 128 }, // Maxwell Generation (SM 5.2) GM20x class
				{ -1, -1 }
			};

		int index = 0;

		while (nGpuArchCoresPerSM[index].SM != -1) {
			if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor)) {
				return nGpuArchCoresPerSM[index].Cores;
			}

			index++;
		}

		// If we don't find the values, we default use the previous one to run properly
		printf("MapSMtoCores for SM %d.%d is undefined.  Default to use %d Cores/SM\n", major, minor, nGpuArchCoresPerSM[index - 1].Cores);
		return nGpuArchCoresPerSM[index - 1].Cores;
	}

	int gpuGetMaxGflopsDeviceId() {
		int current_device = 0, sm_per_multiproc;
		int max_perf_device = 0;
		int device_count = 0, best_SM_arch = 0;
		int devices_prohibited = 0;

		unsigned long long max_compute_perf = 0;
		cudaDeviceProp deviceProp;
		cudaGetDeviceCount(&device_count);

		checkCudaErrors(cudaGetDeviceCount(&device_count));

		if (device_count == 0) {
			fprintf(stderr, "gpuGetMaxGflopsDeviceId() CUDA error: no devices supporting CUDA.\n");
			exit(EXIT_FAILURE);
		}

		// Find the best major SM Architecture GPU device
		while (current_device < device_count) {
			cudaGetDeviceProperties(&deviceProp, current_device);

			// If this GPU is not running on Compute Mode prohibited, then we can add it to the list
			if (deviceProp.computeMode != cudaComputeModeProhibited) {
				if (deviceProp.major > 0 && deviceProp.major < 9999) {
					best_SM_arch = MAX(best_SM_arch, deviceProp.major);
				}
			} else {
				devices_prohibited++;
			}

			current_device++;
		}

		if (devices_prohibited == device_count) {
			fprintf(stderr, "gpuGetMaxGflopsDeviceId() CUDA error: all devices have compute mode prohibited.\n");
			exit(EXIT_FAILURE);
		}

		// Find the best CUDA capable GPU device
		current_device = 0;

		while (current_device < device_count) {
			cudaGetDeviceProperties(&deviceProp, current_device);

			// If this GPU is not running on Compute Mode prohibited, then we can add it to the list
			if (deviceProp.computeMode != cudaComputeModeProhibited) {
				if (deviceProp.major == 9999 && deviceProp.minor == 9999) {
					sm_per_multiproc = 1;
				} else {
					sm_per_multiproc = _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor);
				}

				unsigned long long compute_perf = (unsigned long long) deviceProp.multiProcessorCount * sm_per_multiproc * deviceProp.clockRate;

				if (compute_perf > max_compute_perf) {
					// If we find GPU with SM major > 2, search only these
					if (best_SM_arch > 2) {
						// If our device==dest_SM_arch, choose this, or else pass
						if (deviceProp.major == best_SM_arch) {
							max_compute_perf = compute_perf;
							max_perf_device = current_device;
						}
					} else {
						max_compute_perf = compute_perf;
						max_perf_device = current_device;
					}
				}
			}

			++current_device;
		}

		return max_perf_device;
	}


	int GetEncoderClsid(const WCHAR* format, CLSID* pClsid) {
		UINT num = 0; // number of image encoders
		UINT size = 0; // size of the image encoder array in bytes

		Gdiplus::ImageCodecInfo* pImageCodecInfo;

		Gdiplus::GetImageEncodersSize(&num, &size);
		if (size == 0)
			return -1; // Failure

		pImageCodecInfo = (Gdiplus::ImageCodecInfo*)(malloc(size));
		if (pImageCodecInfo == nullptr)
			return -1; // Failure

		GetImageEncoders(num, size, pImageCodecInfo);

		for (UINT j = 0; j < num; ++j) {
			if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0) {
				*pClsid = pImageCodecInfo[j].Clsid;
				free(pImageCodecInfo);
				return j; // Success
			}
		}

		free(pImageCodecInfo);
		return -1; // Failure
	}

	bool CaptureScreenShot(int nWidth, int nHeight, const std::wstring& szDestFile, const std::wstring& szEncoderString) {
		UINT* pixels = new UINT[nWidth * nHeight];
		memset(pixels, 0, sizeof(UINT) * nWidth * nHeight);

		wchar_t FullPath[MAX_PATH];
		memset(FullPath, 0, sizeof(FullPath));
		std::wstring szExePath;
		if (::GetModuleFileNameW(nullptr, FullPath, sizeof(wchar_t) * MAX_PATH)) {
			szExePath = FullPath;

			int pos = szExePath.rfind(L'\\');

			if (-1 != pos) {
				szExePath = szExePath.substr(0, pos + 1);
			}
		}

		std::wstring szDest = szExePath + szDestFile;

		//glFlush(); glFinish();

		glReadPixels(0, 0, nWidth, nHeight, GL_BGRA_EXT, GL_UNSIGNED_BYTE, pixels);

		if (NULL == pixels)
			return false;

		// Initialize GDI+
		Gdiplus::GdiplusStartupInput gdiplusStartupInput;
		ULONG_PTR gdiplusToken;
		Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, nullptr);
		// Create the dest image
		{
			Gdiplus::Bitmap DestBmp(nWidth, nHeight, PixelFormat32bppARGB);

			Gdiplus::Rect rect1(0, 0, nWidth, nHeight);

			Gdiplus::BitmapData bitmapData;
			memset(&bitmapData, 0, sizeof(bitmapData));
			DestBmp.LockBits(
				&rect1,
				Gdiplus::ImageLockModeRead,
				PixelFormat32bppARGB,
				&bitmapData);

			int nStride1 = bitmapData.Stride;
			if (nStride1 < 0)
				nStride1 = -nStride1;

			UINT* DestPixels = (UINT*)bitmapData.Scan0;

			if (!DestPixels) {
				delete[] pixels;
				return false;
			}

			for (UINT row = 0; row < bitmapData.Height; ++row) {
				for (UINT col = 0; col < bitmapData.Width; ++col) {
					DestPixels[row * nStride1 / 4 + col] = pixels[row * nWidth + col];
				}
			}

			DestBmp.UnlockBits(
				&bitmapData);

			delete[] pixels;

			DestBmp.RotateFlip(Gdiplus::RotateNoneFlipY);

			CLSID Clsid;
			int result = GetEncoderClsid(szEncoderString.c_str(), &Clsid);

			if (result < 0)
				return false;

			Gdiplus::Status status = DestBmp.Save(szDest.c_str(), &Clsid);
			if (status != Gdiplus::Ok)
				return false;
		}
		// Shutdown GDI+
		Gdiplus::GdiplusShutdown(gdiplusToken);

		return true;
	}

	inline int gpuGLDeviceInit(int devID) {
		int deviceCount;
		checkCudaErrors(cudaGetDeviceCount(&deviceCount));

		if (deviceCount == 0) {
			fprintf(stderr, "CUDA error: no devices supporting CUDA.\n");
			exit(EXIT_FAILURE);
		}

		if (devID < 0) {
			devID = 0;
		}

		if (devID > deviceCount - 1) {
			fprintf(stderr, "\n");
			fprintf(stderr, ">> %d CUDA capable GPU device(s) detected. <<\n", deviceCount);
			fprintf(stderr, ">> gpuGLDeviceInit (-device=%d) is not a valid GPU device. <<\n", devID);
			fprintf(stderr, "\n");
			return -devID;
		}

		cudaDeviceProp deviceProp;
		checkCudaErrors(cudaGetDeviceProperties(&deviceProp, devID));

		if (deviceProp.computeMode == cudaComputeModeProhibited) {
			fprintf(stderr, "Error: device is running in <Compute Mode Prohibited>, no threads can use ::cudaSetDevice().\n");
			return -1;
		}

		if (deviceProp.major < 1) {
			fprintf(stderr, "Error: device does not support CUDA.\n");
			exit(EXIT_FAILURE);
		}

		fprintf(stderr, "Using device %d: %s\n", devID, deviceProp.name);

		checkCudaErrors(cudaSetDevice(devID));
		checkCudaErrors(cudaGLSetGLDevice(devID));
		return devID;
	}
}
